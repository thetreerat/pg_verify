from PostgresServer import PostgresServer
import sys
if __name__ == "__main__":
    a = PostgresServer()
    b = PostgresServer()
    #a.add_key_pair("allow_system_table_mods",'off')
    #a.add_key_pair('application_name','psql')
    #a.add_key_pair('work_mem','170MB')

    a.load_running_file('showall.txt')
    #a.print_object()

    b.load_config_file('postgresql.conf.openscg', True)
    #b.print_object()
    print """
start of compare
================
"""
    if a.allow_system_table_mods!=b.allow_system_table_mods:
        print """allow_system_table_mods: %s!=%s""" % (a.allow_system_table_mods,b.allow_system_table_mods)
    #if a.application_name!=b.application_name:
    #    print """application_name: %s!=%s""" % (a.application_name, b.application_name)
    if a.archive_command!=b.archive_command:
        print """archive_command: %s!=%s""" % (a.archive_command,a.archive_command)
    if a.archive_mode!=b.archive_mode:
        print """archive_mode: %s!=%s """ % (a.archive_mode,b.archive_mode)
    if a.data_directory!=b.data_directory:
        print """data_directory: %s!=%s""" % (a.data_directory,b.data_directory)
    if a.checkpoint_completion_target!=b.checkpoint_completion_target:
        print """Checkpoint_completion_target: %s!=%s""" % (a.checkpoint_completion_target, b.checkpoint_completion_target)
    if a.checkpoint_segments!=b.checkpoint_segments:
        print """checkpoint_segments: %s!=%s""" % (a.checkpoint_segments, b.checkpoint_segments)
    if a.data_directory!=b.data_directory:
        print """data_directory: %s!=%s""" % (a.data_directory, b.data_directory)
    if a.DateStyle!=b.DateStyle:
        print """DataStyle: %s!=%s""" % (a.DateStyle,b.DateStyle)
    if a.default_text_search_config!=b.default_text_search_config:
        print """defalut_test_search_cinfig %s!=%s""" % (a.default_text_search_config, b.default_text_search_config)
    if a.dynamic_shared_memory_type!=b.dynamic_shared_memory_type:
        print """dynamic_shared_memory_type: %s!=%s""" % (a.dynamic_shared_memory_type, b.dynamic_shared_memory_type)
    if a.effective_cache_size!=b.effective_cache_size:
        print """effective_cache_size: %s!=%s""" % (a.effective_cache_size, b.effective_cache_size)
    if a.external_pid_file!=b.external_pid_file:
        print """external_pid_file: %s!=%s""" % (a.external_pid_file, b.external_pid_file)
    if a.full_page_writes!=b.full_page_writes:
        print """full_page_writes: %s!=%s""" % (a.full_page_writes, b.full_page_writes)
    if a.hba_file!=b.hba_file:
        print """hba_file: %s!=%s""" % (a.hba_file, b.hba_file)
    if a.hot_standby!=b.hot_standby:
        print """hot_standby: %s!=%s""" % (a.hot_standby, b.hot_standby)
    if a.ident_file!=b.ident_file:
        print """ident_file: %s!=%s""" % (a.ident_file, b.ident_file)
    if a.lc_time!=b.lc_time:
        print """lc_time: %s!=%s""" % (a.lc_time, b.lc_time)
    if a.lc_messages!=b.lc_messages:
        print """lc_message: %s!=%s""" % (a.lc_messages, b.lc_messages)
    if a.lc_monetary!=b.lc_monetary:
        print """lc_monetary: %s!=%s""" % (a.lc_monetary,b.lc_monetary)
    if a.lc_numeric!=b.lc_numeric:
        print """lc_monetary: %s!=%s""" % (a.lc_monetary, b.lc_monetary)
    if a.listen_addresses!=b.listen_addresses:
        print """listen_addresses: %s!=%s""" % (a.listen_addresses, b.listen_addresses)
    if a.log_statement!=b.log_statement:
        print """log_statement: %s!=%s""" % (a.log_statement, b.log_statement)
    if a.log_destination!=b.log_destination:
        print """log_destination: %s!=%s""" % (a.log_destination, b.log_destination)
    if a.log_min_duration_statement!=b.log_min_duration_statement:
        print """log_min_duration_statment: %s!=%s""" % (a.log_min_duration_statement, b.log_min_duration_statement)
    if a.log_line_prefix!=b.log_line_prefix:
        print """log_line_prefix: %s!=%s""" % (a.log_line_prefix, b.log_line_prefix)
    if a.log_lock_waits!=b.log_lock_waits:
        print """log_lock_waits: %s!=%s""" % (a.log_lock_waits, b.log_lock_waits)
    if a.log_statement!=b.log_statement:
        print """log_statement: %s!=%s""" % (a.log_statement, b.log_statement)
    if a.log_timezone!=b.log_timezone:
        print """log_timezone: %s!=%s""" % (a.log_timezone, b.log_timezone)
    if a.maintenance_work_mem!=b.maintenance_work_mem:
        print """maintenace_work_mem: %s!=%s""" % (a.maintenance_work_mem, b.maintenance_work_mem)
    if a.max_connections!=b.max_connections:
        print """max_connections: %s!=%s""" % (a.maintenance_work_mem, b.maintenance_work_mem)
    if a.max_stack_depth!=b.max_stack_depth:
        print """max_stack_depth: %s!=%s""" % (a.max_stack_depth,b.max_stack_depth)
    if a.max_files_per_process!=b.max_files_per_process:
        print """max_files_per_process: %s!=%s""" % (a.max_files_per_process, b.max_files_per_process)
    if a.max_wal_senders!=b.max_wal_senders:
        print """max_wal_senders: %s!=%s""" % (a.max_wal_senders, b.max_wal_senders)
    if a.port!=b.port:
        print """port: %s!=%s""" % (a.port, b.port)
    if a.shared_buffers!=b.shared_buffers:
        print """shared_buffers: %s!=%s""" % (a.shared_buffers, b.shared_buffers)
    if a.shared_preload_libraries!=b.shared_preload_libraries:
        print """shared_preload_libraries: %s!=$s""" % (a.shared_preload_libraries, b.shared_preload_libraries)
    if a.ssl_cert_file!=b.ssl_cert_file:
        print """ssl_cert_file: %s!=%s""" % (a.ssl_cert_file, b.ssl_cert_file)
    if a.stats_temp_directory!=b.stats_temp_directory:
        print """stats_temp_directory: %s!=%s""" % (a.stats_temp_directory, b.stats_temp_directory)
    if a.temp_buffers!=b.temp_buffers:
        print """temp_buffers: %s!=%s""" % (a.temp_buffers, b.temp_buffers)
    if a.timezone!=b.timezone:
        print """timezone: %s!=%s""" % (a.timezone,b.timezone)
    if a.track_functions!=b.track_functions:
        print """track_functions: %s!=%s""" % (a.track_functions,b.track_functions)
    if a.track_activities!=b.track_activities:
        print """track_activites: %s!=%s""" % (a.track_activities, b.track_activities)
    if a.unix_socket_directories!=b.unix_socket_directories:
        print """unix_socket_directories: %s!=%s""" % (a.unix_socket_directories, b.unix_socket_directories)
    if a.wal_keep_segments!=b.wal_keep_segments:
        print """wal_keep_segments: %s!=%s""" % (a.wal_keep_segments, b.wal_keep_segments)
    if a.wal_level!=b.wal_level:
        print """wal_level: %s!=%s""" %  (a.wal_level,b.wal_level)
    if a.work_mem!=b.work_mem:
        print """work_mem: %s!=%s""" % (a.work_mem,b.work_mem)
    print """
end of compare
==============
"""