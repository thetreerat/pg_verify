class PostgresServer(object):
  """Class for storing Server Configs"""
  def __init__(self,
               allow_system_table_mods=None,
               application_name=None,
               archive_command=None,
               archive_mode=None,
               archive_timeout=None,
               array_nulls=None,
               authentication_timeout=None,
               autovacuum=None,
               autovacuum_analyze_scale_factor=None,
               autovacuum_analyze_threshold=None,
               autovacuum_freeze_max_age=None,
               autovacuum_max_workers=None,
               autovacuum_multixact_freeze_max_age=None,
               autovacuum_naptime=None,
               autovacuum_vacuum_cost_delay=None,
               autovacuum_vacuum_cost_limit=None,
               autovacuum_vacuum_scale_factor=None,
               autovacuum_vacuum_threshold=None,
               autovacuum_work_mem=None,
               backslash_quote=None,
               bgwriter_delay=None,
               bgwriter_lru_maxpages=None,
               bgwriter_lru_multiplier=None,
               block_size=None,
               bonjour=None,
               bonjour_name = None,        
               bytea_output = None,        
               check_function_bodies = None,
               checkpoint_completion_target = None,
               checkpoint_segments = None,
               checkpoint_timeout = None,
               checkpoint_warning = None,
               client_encoding = None,
               client_min_messages = None, 
               commit_delay = None,       
               commit_siblings = None,    
               config_file = None,        
               constraint_exclusion = None,
               cpu_index_tuple_cost = None,
               cpu_operator_cost = None,   
               cpu_tuple_cost = None,      
               cursor_tuple_fraction = None,
               data_checksums = None,       
               data_directory = None,       
               DateStyle = None,            
               db_user_namespace = None,    
               deadlock_timeout = None,     
               debug_assertions = None,     
               debug_print_parse = None,    
               debug_print_plan = None,     
               debug_print_rewritten = None,
               default_statistics_target = None, 
               default_tablespace = None,        
               default_text_search_config = None,
               default_transaction_deferrable = None,
               default_transaction_isolation = None, 
               default_transaction_read_only = None, 
               default_with_oids = None,             
               dynamic_library_path = None,          
               dynamic_shared_memory_type = None,    
               effective_cache_size = None,          
               effective_io_concurrency = None,      
               enable_bitmapscan = None,             
               enable_hashagg = None,                
               enable_hashjoin = None,               
               enable_indexscan = None,              
               enable_material = None,         
               enable_mergejoin = None,              
               enable_nestloop = None,
               enable_seqscan = None, 
               enable_sort = None,
               enable_tidscan = None,
               escape_string_warning = None,
               event_source = None,         
               exit_on_error = None,        
               external_pid_file = None,    
               extra_float_digits = None,   
               from_collapse_limit = None,  
               fsync = None,                
               full_page_writes = None,     
               geqo = None,                 
               geqo_effort = None,          
               geqo_generations = None,     
               geqo_pool_size = None,       
               geqo_seed = None,            
               geqo_selection_bias = None,  
               geqo_threshold = None,       
               gin_fuzzy_search_limit = None,
               hba_file = None,              
               hot_standby = None,           
               hot_standby_feedback = None, 
               huge_pages = None,            
               ident_file = None,            
               ignore_checksum_failure = None,
               ignore_system_indexes = None,  
               integer_datetimes = None,      
               IntervalStyle = None,          
               join_collapse_limit = None,    
               krb_caseins_users = None,      
               krb_server_keyfile = None,     
               lc_collate = None,             
               lc_ctype = None,               
               lc_messages = None,            
               lc_monetary = None,            
               lc_numeric = None,             
               lc_time = None,                 
               listen_addresses = None,        
               lo_compat_privileges = None,    
               local_preload_libraries = None, 
               lock_timeout = None,            
               log_autovacuum_min_duration = None, 
               log_checkpoints = None,             
               log_connections = None,             
               log_destination = None,             
               log_directory = None,               
               log_disconnections = None,          
               log_duration = None,                
               log_error_verbosity = None,         
               log_executor_stats = None,          
               log_file_mode = None,               
               log_filename = None,                
               log_hostname = None,                
               log_line_prefix = None,             
               log_lock_waits = None,              
               log_min_duration_statement = None,  
               log_min_error_statement = None,     
               log_min_messages = None,            
               log_parser_stats = None,            
               log_planner_stats = None,           
               log_rotation_age = None,            
               log_rotation_size = None,           
               log_statement = None,               
               log_statement_stats = None,         
               log_temp_files = None,              
               log_timezone = None,
               log_truncate_on_rotation = None,    
               logging_collector = None,           
               maintenance_work_mem = None,        
               max_connections = None,             
               max_files_per_process = None,       
               max_function_args = None,           
               max_identifier_length = None,       
               max_index_keys = None,              
               max_locks_per_transaction = None,   
               max_pred_locks_per_transaction = None,
               max_prepared_transactions = None,     
               max_replication_slots = None,         
               max_stack_depth = None,               
               max_standby_archive_delay = None,     
               max_standby_streaming_delay = None,   
               max_wal_senders = None,               
               max_worker_processes = None,          
               password_encryption = None,           
               pg_stat_statements_max = None,        
               pg_stat_statements_save = None,       
               pg_stat_statements_track = None,      
               pg_stat_statements_track_utility = None, 
               port = None,                             
               post_auth_delay = None,                  
               pre_auth_delay = None,                   
               quote_all_identifiers = None,            
               random_page_cost = None,                 
               restart_after_crash = None,              
               search_path = None,                      
               segment_size = None,                     
               seq_page_cost = None,                    
               server_encoding = None,                  
               server_version = None,                   
               server_version_num = None,               
               session_preload_libraries = None,        
               session_replication_role = None,         
               shared_buffers = None,                   
               shared_preload_libraries = None,         
               sql_inheritance = None,                  
               ssl = None,                              
               ssl_ca_file = None,                      
               ssl_cert_file = None,
               ssl_ciphers = None,
               ssl_crl_file = None, 
               ssl_ecdh_curve = None,
               ssl_key_file = None,  
               ssl_prefer_server_ciphers = None,
               ssl_renegotiation_limit = None,  
               standard_conforming_strings = None,
               statement_timeout = None,          
               stats_temp_directory = None,       
               superuser_reserved_connections = None,
               synchronize_seqscans = None,          
               synchronous_commit = None,            
               synchronous_standby_names = None,     
               syslog_facility = None,               
               syslog_ident = None,                  
               tcp_keepalives_count = None,          
               tcp_keepalives_idle = None,           
               tcp_keepalives_interval = None,       
               temp_buffers = None,                  
               temp_file_limit = None,               
               temp_tablespaces = None,              
               timezone = None,                      
               timezone_abbreviations = None,        
               trace_notify = None,                  
               trace_recovery_messages = None,       
               trace_sort = None,                    
               track_activities = None,              
               track_activity_query_size = None,     
               track_counts = None,
               track_functions = None,
               track_io_timing = None,
               transaction_deferrable = None,
               transaction_isolation = None, 
               transaction_read_only = None, 
               transform_null_equals = None, 
               unix_socket_directories = None,
               unix_socket_group = None,      
               unix_socket_permissions = None,
               update_process_title = None,   
               vacuum_cost_delay = None,      
               vacuum_cost_limit = None,      
               vacuum_cost_page_dirty = None, 
               vacuum_cost_page_hit = None,   
               vacuum_cost_page_miss = None,  
               vacuum_defer_cleanup_age = None,
               vacuum_freeze_min_age = None,   
               vacuum_freeze_table_age = None, 
               vacuum_multixact_freeze_min_age = None,
               vacuum_multixact_freeze_table_age = None,
               wal_block_size  = None,               
               wal_buffers = None,
               wal_keep_segments = None,
               wal_level = None,                        
               wal_log_hints = None,                    
               wal_receiver_status_interval = None,     
               wal_receiver_timeout = None,             
               wal_segment_size = None,                 
               wal_sender_timeout = None,               
               wal_sync_method = None,                  
               wal_writer_delay = None,                 
               work_mem = None,                         
               xmlbinary = None,                        
               xmloption = None,                        
               zero_damaged_pages = None):
    """Create Object and load"""
    self.allow_system_table_mods = allow_system_table_mods
    self.application_name = application_name
    self.archive_command = archive_command
    self.archive_mode = archive_mode
    self.archive_timeout = archive_timeout 
    self.array_nulls = array_nulls
    self.authentication_timeout = authentication_timeout
    self.autovacuum = autovacuum
    self.autovacuum_analyze_scale_factor =autovacuum_analyze_scale_factor
    self.autovacuum_analyze_threshold = autovacuum_analyze_threshold
    self.autovacuum_freeze_max_age = autovacuum_freeze_max_age
    self.autovacuum_max_workers =autovacuum_max_workers
    self.autovacuum_multixact_freeze_max_age = autovacuum_multixact_freeze_max_age
    self.autovacuum_naptime = autovacuum_naptime
    self.autovacuum_vacuum_cost_delay = autovacuum_vacuum_cost_delay
    self.autovacuum_vacuum_cost_limit = autovacuum_vacuum_cost_limit
    self.autovacuum_vacuum_scale_factor = autovacuum_vacuum_scale_factor
    self.autovacuum_vacuum_threshold = autovacuum_vacuum_threshold
    self.autovacuum_work_mem = autovacuum_work_mem
    self.backslash_quote = backslash_quote
    self.bgwriter_delay = bgwriter_delay
    self.bgwriter_lru_maxpages = bgwriter_lru_maxpages
    self.bgwriter_lru_multiplier = bgwriter_lru_multiplier
    self.block_size = block_size          
    self.bonjour = bonjour             
    self.bonjour_name = bonjour_name       
    self.bytea_output =bytea_output       
    self.check_function_bodies= check_function_bodies
    self.checkpoint_completion_target = checkpoint_completion_target
    self.checkpoint_segments = checkpoint_segments
    self.checkpoint_timeout = checkpoint_timeout
    self.checkpoint_warning = checkpoint_warning
    self.client_encoding = client_encoding
    self.client_min_messages = client_min_messages 
    self.commit_delay = commit_delay
    self.commit_siblings = commit_siblings    
    self.config_file = config_file        
    self.constraint_exclusion = constraint_exclusion
    self.cpu_index_tuple_cost = cpu_index_tuple_cost
    self.cpu_operator_cost = cpu_operator_cost  
    self.cpu_tuple_cost = cpu_tuple_cost      
    self.cursor_tuple_fraction = cursor_tuple_fraction
    self.data_checksums = data_checksums       
    self.data_directory = data_directory       
    self.DateStyle = DateStyle            
    self.db_user_namespace = db_user_namespace    
    self.deadlock_timeout = deadlock_timeout     
    self.debug_assertions = debug_assertions     
    self.debug_print_parse = debug_print_parse    
    self.debug_print_plan = debug_print_plan     
    self.debug_print_rewritten = debug_print_rewritten 
    self.default_statistics_target = default_statistics_target
    self.default_tablespace = default_tablespace        
    self.default_text_search_config = default_text_search_config
    self.default_transaction_deferrable = default_transaction_deferrable
    self.default_transaction_isolation = default_transaction_isolation 
    self.default_transaction_read_only = default_transaction_read_only 
    self.default_with_oids = default_with_oids             
    self.dynamic_library_path = dynamic_library_path           
    self.dynamic_shared_memory_type = dynamic_shared_memory_type    
    self.effective_cache_size = effective_cache_size          
    self.effective_io_concurrency = effective_io_concurrency      
    self.enable_bitmapscan = enable_bitmapscan             
    self.enable_hashagg = enable_hashagg                
    self.enable_hashjoin = enable_hashjoin               
    self.enable_indexscan = enable_indexscan              
    self.enable_material = enable_material               
    self.enable_mergejoin = enable_mergejoin             
    self.enable_nestloop = enable_nestloop
    self.enable_seqscan = enable_seqscan
    self.enable_sort = enable_sort
    self.enable_tidscan = enable_tidscan
    self.escape_string_warning = escape_string_warning
    self.event_source = event_source         
    self.exit_on_error = exit_on_error        
    self.external_pid_file = external_pid_file   
    self.extra_float_digits = extra_float_digits   
    self.from_collapse_limit = from_collapse_limit  
    self.fsync = fsync                
    self.full_page_writes = full_page_writes     
    self.geqo = geqo                 
    self.geqo_effort = geqo_effort          
    self.geqo_generations = geqo_generations     
    self.geqo_pool_size =geqo_pool_size       
    self.geqo_seed = geqo_seed            
    self.geqo_selection_bias = geqo_selection_bias
    self.geqo_threshold = geqo_threshold        
    self.gin_fuzzy_search_limit = gin_fuzzy_search_limit
    self.hba_file = hba_file              
    self.hot_standby = hot_standby           
    self.hot_standby_feedback = hot_standby_feedback  
    self.huge_pages = huge_pages            
    self.ident_file = ident_file            
    self.ignore_checksum_failure = ignore_checksum_failure
    self.ignore_system_indexes = ignore_system_indexes 
    self.integer_datetimes = integer_datetimes      
    self.IntervalStyle = IntervalStyle          
    self.join_collapse_limit = join_collapse_limit    
    self.krb_caseins_users = krb_caseins_users  
    self.krb_server_keyfile = krb_server_keyfile     
    self.lc_collate = lc_collate             
    self.lc_ctype = lc_ctype               
    self.lc_messages = lc_messages            
    self.lc_monetary = lc_monetary            
    self.lc_numeric = lc_numeric             
    self.lc_time = lc_time                 
    self.listen_addresses = listen_addresses        
    self.lo_compat_privileges = lo_compat_privileges    
    self.local_preload_libraries = local_preload_libraries
    self.lock_timeout = lock_timeout            
    self.log_autovacuum_min_duration = log_autovacuum_min_duration 
    self.log_checkpoints = log_checkpoints             
    self.log_connections = log_connections             
    self.log_destination = log_destination
    self.log_directory = log_directory               
    self.log_disconnections = log_disconnections          
    self.log_duration = log_duration                
    self.log_error_verbosity = log_error_verbosity         
    self.log_executor_stats = log_executor_stats          
    self.log_file_mode = log_file_mode               
    self.log_filename = log_filename                
    self.log_hostname = log_hostname                
    self.log_line_prefix = log_line_prefix             
    self.log_lock_waits = log_lock_waits              
    self.log_min_duration_statement = log_min_duration_statement 
    self.log_min_error_statement = log_min_error_statement     
    self.log_min_messages = log_min_messages            
    self.log_parser_stats = log_parser_stats            
    self.log_planner_stats = log_planner_stats           
    self.log_rotation_age = log_planner_stats            
    self.log_rotation_size = log_rotation_size           
    self.log_statement = log_statement
    self.log_statement_stats = log_statement_stats
    self.log_temp_files = log_temp_files            
    self.log_timezone = log_timezone                
    self.log_truncate_on_rotation = log_truncate_on_rotation    
    self.logging_collector = logging_collector           
    self.maintenance_work_mem = maintenance_work_mem        
    self.max_connections = max_connections              
    self.max_files_per_process = max_files_per_process       
    self.max_function_args = max_function_args           
    self.max_identifier_length = max_identifier_length       
    self.max_index_keys = max_index_keys              
    self.max_locks_per_transaction = max_locks_per_transaction   
    self.max_pred_locks_per_transaction = max_pred_locks_per_transaction
    self.max_prepared_transactions = max_prepared_transactions     
    self.max_replication_slots = max_replication_slots         
    self.max_stack_depth = max_stack_depth                
    self.max_standby_archive_delay = max_standby_archive_delay     
    self.max_standby_streaming_delay = max_standby_streaming_delay   
    self.max_wal_senders = max_wal_senders               
    self.max_worker_processes = max_worker_processes          
    self.password_encryption = password_encryption           
    self.pg_stat_statements_max = pg_stat_statements_max        
    self.pg_stat_statements_save = pg_stat_statements_save       
    self.pg_stat_statements_track = pg_stat_statements_track      
    self.pg_stat_statements_track_utility  = pg_stat_statements_track_utility
    self.port = port                             
    self.post_auth_delay = post_auth_delay                  
    self.pre_auth_delay = pre_auth_delay                   
    self.quote_all_identifiers = quote_all_identifiers            
    self.random_page_cost = random_page_cost                
    self.restart_after_crash = restart_after_crash              
    self.search_path = search_path                     
    self.segment_size = segment_size                     
    self.seq_page_cost = seq_page_cost                    
    self.server_encoding = server_encoding                  
    self.server_version = server_version                  
    self.server_version_num = server_version_num              
    self.session_preload_libraries = session_preload_libraries        
    self.session_replication_role = session_replication_role         
    self.shared_buffers = shared_buffers                   
    self.shared_preload_libraries =shared_preload_libraries         
    self.sql_inheritance = sql_inheritance                  
    self.ssl = ssl                              
    self.ssl_ca_file = ssl_ca_file                      
    self.ssl_cert_file = ssl_cert_file
    self.ssl_ciphers = ssl_ciphers
    self.ssl_crl_file = ssl_crl_file
    self.ssl_ecdh_curve = ssl_ecdh_curve
    self.ssl_key_file = ssl_key_file  
    self.ssl_prefer_server_ciphers = ssl_prefer_server_ciphers
    self.ssl_renegotiation_limit = ssl_renegotiation_limit  
    self.standard_conforming_strings = standard_conforming_strings
    self.statement_timeout = statement_timeout          
    self.stats_temp_directory = stats_temp_directory       
    self.superuser_reserved_connections = superuser_reserved_connections
    self.synchronize_seqscans = synchronize_seqscans          
    self.synchronous_commit = synchronous_commit            
    self.synchronous_standby_names = synchronous_standby_names      
    self.syslog_facility  = syslog_facility              
    self.syslog_ident = syslog_ident                  
    self.tcp_keepalives_count = tcp_keepalives_count          
    self.tcp_keepalives_idle = tcp_keepalives_idle           
    self.tcp_keepalives_interval = tcp_keepalives_interval       
    self.temp_buffers = temp_buffers                  
    self.temp_file_limit = temp_file_limit               
    self.temp_tablespaces = temp_tablespaces              
    self.timezone = timezone                      
    self.timezone_abbreviations = timezone_abbreviations        
    self.trace_notify = trace_notify                  
    self.trace_recovery_messages = trace_recovery_messages       
    self.trace_sort = trace_sort                    
    self.track_activities = track_activities              
    self.track_activity_query_size = track_activity_query_size     
    self.track_counts = track_counts
    self.track_functions = track_functions
    self.track_io_timing = track_io_timing
    self.transaction_deferrable = transaction_deferrable
    self.transaction_isolation = transaction_isolation 
    self.transaction_read_only = transaction_read_only 
    self.transform_null_equals = transform_null_equals 
    self.unix_socket_directories = unix_socket_directories
    self.unix_socket_group = unix_socket_group      
    self.unix_socket_permissions = unix_socket_permissions
    self.update_process_title = update_process_title
    self.vacuum_cost_delay = vacuum_cost_delay      
    self.vacuum_cost_limit = vacuum_cost_limit      
    self.vacuum_cost_page_dirty = vacuum_cost_page_dirty 
    self.vacuum_cost_page_hit = vacuum_cost_page_hit   
    self.vacuum_cost_page_miss  = vacuum_cost_page_miss
    self.vacuum_defer_cleanup_age = vacuum_defer_cleanup_age
    self.vacuum_freeze_min_age = vacuum_freeze_min_age   
    self.vacuum_freeze_table_age = vacuum_freeze_table_age 
    self.vacuum_multixact_freeze_min_age = vacuum_multixact_freeze_min_age 
    self.vacuum_multixact_freeze_table_age = vacuum_multixact_freeze_table_age
    self.wal_block_size = wal_block_size                   
    self.wal_buffers = wal_buffers                      
    self.wal_keep_segments = wal_keep_segments                
    self.wal_level = wal_level                        
    self.wal_log_hints = wal_log_hints                    
    self.wal_receiver_status_interval = wal_receiver_status_interval     
    self.wal_receiver_timeout = wal_receiver_timeout
    self.wal_segment_size = wal_segment_size
    self.wal_sender_timeout = wal_sender_timeout
    self.wal_sync_method = wal_sync_method
    self.wal_writer_delay = wal_writer_delay
    self.work_mem = work_mem
    self.xmlbinary = xmlbinary                        
    self.xmloption = xmloption
    self.zero_damaged_pages = zero_damaged_pages
  
  
  def add_key_pair(self,key,value, debug=False):
    """Load key values"""
    if key=="allow_system_table_mods":
      self.allow_system_table_mods=value
    elif key=="application_name":
      self.application_name=value
    elif key=="archive_command":
      self.archive_command=value
    elif key=="archive_mode":
      self.archive_mode = value
    elif key=="checkpoint_completion_target":
      self.checkpoint_completion_target = value
    elif key=="checkpoint_segments":
      self.checkpoint_segments = value
    elif key=="data_directory":
      self.data_directory=value
    elif key=="datestyle":
      self.DateStyle = value
    elif key=="default_text_search_config":
      self.default_text_search_config=value
    elif key=="dynamic_shared_memory_type":
      self.dynamic_shared_memory_type = value
    elif key=="effective_cache_size":
      self.effective_cache_size = value
    elif key=="external_pid_file":
      self.external_pid_file = value
    elif key=="full_page_writes":
      self.full_page_writes = value
    elif key=="hba_file":
      self.hba_file = value
    elif key=="hot_standby":
      self.hot_standby = value
    elif key=="ident_file":
      self.ident_file = value
    elif key=="lc_time":
      self.lc_time=value
    elif key=="lc_messages":
      self.lc_messages = value
    elif key=="lc_monetary":
      self.lc_monetary=value
    elif key=="lc_numeric":
      self.lc_numeric=value
    elif key=="listen_addresses":
      self.listen_addresses = value
    elif key=="log_destination":
      self.log_destination = value
    elif key=="log_min_duration_statement":
      self.log_min_duration_statement = value
    elif key=="log_line_prefix":
      self.log_line_prefix = value
    elif key=="log_lock_waits":
      self.log_lock_waits = value
    elif key=="log_statement":
      self.log_statement =value
    elif key=="log_timezone":
      self.log_timezone = value
    elif key=="maintenance_work_mem":
      self.maintenance_work_mem = value
    elif key=="max_connections":
      self.max_connections = value
    elif key=="max_stack_depth":
      self.max_stack_depth = value
    elif key=="max_wal_senders":
      self.max_wal_senders = value
    elif key=="port":
      self.port = value
    elif key=="shared_buffers":
      self.shared_buffers = value
    elif key=="shared_preload_libraries":
      self.shared_preload_libraries = value
    elif key=="ssl_cert_file":
      self.ssl_cert_file = value
    elif key=="ssl_key_file":
      self.ssl_key_file = value      
    elif key=="stats_temp_directory":
      self.stats_temp_directory = value
    elif key=="temp_buffers":
      self.temp_buffers = value
    elif key=="timezone":
      self.timezone=value
    elif key=="track_functions":
      self.track_functions = value
    elif key=="track_activity_query_size":
      self.track_activity_query_size = value
    elif key=="unix_socket_directories":
      self.unix_socket_directories = value
    elif key=="wal_keep_segments":
      self.wal_keep_segments = value
    elif key=="wal_level":
      self.wal_level = value
    elif key=="wal_sender_timeout":
      self.wal_sender_timeout = value
    elif key=="work_mem":
      self.work_mem=value
    else:
      if debug==True:
        print key
        
        
  def load_config_file(self, filename, debug=False):
    """Loads standard config file into object"""
    with open(filename, 'r+') as configfile:
        hit = None
        for line in configfile:
            line = line.strip()
            if line[:1]=='#':
                hit = 'pound'
            elif len(line)==0:
                hit = 'blank'
            else:
                e = line.find('=')
                key = line[:e-1].strip()
                value = line[e+1:].strip()
                test = value.find('#')
                if test!=-1:
                    value = value[:test-1]
                while value[-1:]==chr(9):
                  value = value[:-1]
                if value[:1]==chr(39):
                  value = value[1:]
                if value[-1:]==chr(39):
                  value = value[:-1]
                self.add_key_pair(key,value, debug)

    
  def load_running_file(self, filename, debug=False):
    """Load output from running Postgres server"""
    with open(filename, 'r+') as repofile:
        hit = None
        for line in repofile:
            line = line.strip().lower()
            #print line[:3]
            if line[:4]=='name':
                hit='name'
            elif line[:10]=='----------':
                hit='dash'
            elif line[:1]=='(':
                hit='right pren'            
            else:
                key = line[:36].strip()
                value = line[37:96].strip()
                self.add_key_pair(key,value, debug)
      

  def print_object(self):
    """prints out non null values for object"""
    print """
System Settings
===============
"""
    if self.allow_system_table_mods!=None:
      print """allow_system_table_mods: %s""" % (self.allow_system_table_mods)
    if self.application_name!=None:
      print """application_name: %s""" % (self.application_name)
    if self.archive_command!=None:
      print """archive_command: %s""" % (self.archive_command)
    if self.data_directory!=None:
      print """data_directory: %s""" %  (self.data_directory)
    if self.default_text_search_config!=None:
      print """default_text_search_config: %s""" % (self.default_text_search_config)
    if self.lc_time!=None:
      print """lc_time: %s""" % (self.lc_time)
    if self.timezone!=None:
      print """timezone: %s""" % (self.timezone)
    if self.work_mem!=None:
      print """work_mem: %s""" % (self.work_mem)
    print """
===============
End Settings"""
if __name__ == "__main__":
  """Main class funtion"""
  print "your a goober, call this class from your own code."